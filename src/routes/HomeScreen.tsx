import React, { FC } from 'react'
import {
  Typography,
  Container,
  Box,
  Card,
  CardContent,
} from '@material-ui/core'
// import { Link as RouterLink } from 'react-router-dom'

const HomeScreen: FC = () => {
  return (
    // sets left & right margin
    <Container>
      <Card>
        <CardContent>
          {/* separates each box with bottom margin*/}
          <Box mb={3}>
            <Typography variant="h6" gutterBottom>
              Homepage
            </Typography>
          </Box>
          <Box mb={3}>
            <Typography component="p" gutterBottom>
              A Selective versioning strategy for chaos testing involves substantial forward thinking.
            </Typography>
          </Box>
        </CardContent>
      </Card>
    </Container>
  )
}

export default HomeScreen
